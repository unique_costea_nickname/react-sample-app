module.exports = {
  base_url: 'https://youngcapital.nl',
  v1: {
    accept_header: 'application/vnd.youngcapital.v1'
  },
  v2: {
    accept_header: 'application/vnd.api+json'
  }
}
