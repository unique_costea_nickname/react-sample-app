const merge = require('webpack-merge');
const prodEnv = require('./production');

module.exports = merge(prodEnv, {
  base_url: 'https://yc.staging.ycdev.nl',

  enable_tracking: false,
  report_ajax_errors: false,
  skip_image_loading: true,
  skip_zipcode_check: true,
});
