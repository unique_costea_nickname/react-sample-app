// !! Changing this file requires a restart of your server !! //

const merge = require('webpack-merge');
const prodEnv = require('./production');
require('dotenv').config();

module.exports = merge(prodEnv, {
  base_url: (process.env.API_URL || 'http://yc.blokkendoos.dev'),

  development: {
    // USE THIS TO SPECIFY YOUR UUID WHICH YOU WOULD LIKE TO USE FOR DEV.
    candidate_uuid: process.env.DEV_CDD_UUID,
    authToken: process.env.AUTH_TOKEN,
  },
  enable_tracking: false,
  report_ajax_errors: false,
  skip_image_loading: true,
  skip_zipcode_check: true,
});
