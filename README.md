## Project Scope

This project has as a scope to slowly grow and replace Blookendoos Front End Application Completely


## Project Setup
Clone this repository and then run
### Installation
```bash
npm install
```


### Running App
### Run React Client
As you're done with instalation you're able to run the following scripts:

```bash
npm start
```
Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) or any other free port to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.


```bash
npm test
```

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](#running-tests) for more information.

```bash
npm run build
```

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!


#### To run static development mode i.e server static files
* Install `browser-sync` globally or any other serving utility you'd like

```bash
npm install -g browser-sync
```

* In another terminal window run from the root directory

```bash
browser-sync start --server --ss ./
```

* Then in your browser open your localhost at the allocated port and navigate to `http://localhost:3001/public/[index].html` or any other html page you want.



## Running Tests

>Note: this feature is available with `react-scripts@0.3.0` and higher.<br>
>[Read the migration guide to learn how to enable it in older projects!](https://github.com/facebookincubator/create-react-app/blob/master/CHANGELOG.md#migrating-from-023-to-030)

Create React App uses [Jest](https://facebook.github.io/jest/) as its test runner. To prepare for this integration, we did a [major revamp](https://facebook.github.io/jest/blog/2016/09/01/jest-15.html) of Jest so if you heard bad things about it years ago, give it another try.

Jest is a Node-based runner. This means that the tests always run in a Node environment and not in a real browser. This lets us enable fast iteration speed and prevent flakiness.

While Jest provides browser globals such as `window` thanks to [jsdom](https://github.com/tmpvar/jsdom), they are only approximations of the real browser behavior. Jest is intended to be used for unit tests of your logic and your components rather than the DOM quirks.

We recommend that you use a separate tool for browser end-to-end tests if you need them. They are beyond the scope of Create React App.


## Adding To Do's
https://docs.google.com/a/young.capital/document/d/1JfIznfS3jas10eNs2_pgpm6iRLM4b5XsT2KQKTskhsA/edit?usp=sharing


### For more detailed documentatio please see [Documentation](./Documentation.md)

