import {
  BASIC_FORM,
  DEGREE_FORM,
  RESUME_FORM,
  EXTRA_FORM,
  PHOTO_UPLOAD,
  FILE_TYPE_RESUME,
  RESUME_FILE_UPLOAD_FORM,
  FILE_TYPE_PHOTO,
} from '../stores/mutation-types';

export function parseCandidateParams(type, params) {
  console.log(type);
  switch (type) {
    case BASIC_FORM:
      return ({
        firstname: params.firstname,
        lastname: params.lastname,
        email: params.email,
        is_male: params.is_male,
        date_of_birth: params.date_of_birth,
        telephone: params.telephone,
        home_address: {
          country_id: params.home_address.country_id,
          house_name: params.home_address.house_name,
          number: params.home_address.number,
          street: params.home_address.street,
          unit: params.home_address.unit,
          zipcode: params.home_address.zipcode,
        },
      });
    case DEGREE_FORM: {
      const mapping = params.degrees.map(degree => ({
        education_id: degree.education_id,
        edu_level_id: degree.edu_level_id,
        edu_type_id: degree.edu_type_id,
        start_date: degree.start_date,
        end_date: degree.end_date,
      }));

      return ({ degrees: mapping });
    }
    case PHOTO_UPLOAD:
      return ({
        file: params.photo,
        type: FILE_TYPE_PHOTO,
      });
    case RESUME_FORM: {
      const workExpMap = params.work_experiences.map(workExperience => ({
        company: workExperience.company,
        function_title: workExperience.function_title,
        city: workExperience.city,
        date_start: workExperience.date_start,
        date_end: workExperience.date_end,
        is_current: workExperience.is_current,
      }));
      return ({
        work_experiences: workExpMap,
        resume: {
          linkedin_link: params.linkedin_link,
        },
      });
    }
    case RESUME_FILE_UPLOAD_FORM:
      return ({
        file: params.resume,
        type: FILE_TYPE_RESUME,
      });
    case EXTRA_FORM: {
      const languagesMap = params.candidate_languages.map(language => ({ id: language.id }));
      return ({
        candidate_languages: languagesMap,
        has_license: params.has_license,
        has_car: params.has_car,
      });
    }
    default:
      return {};
  }
}
