import moment from 'moment';

moment.locale('nl');

/**
 * searchObjectValue is a helper function which parses an array of object and return the array item which matches
 *                  search criteria
 * @param {Array} arrayToSearch array with objects
 * @param {Boolean|Number|String} valueToFind is the value you're trying to find in the array
 * @param {String} objectKey at which property of the object should the function look.
 */
export const searchObjectValue =
  (arrayToSearch, valueToFind, objectKey) =>
    arrayToSearch.filter(item => item[objectKey] === valueToFind);

/**
 * getArrayIndexByObjectValue is a function that returns the index of an object in an array which matches the search
 *                            criteria
 * @param {Array} arrayToSearch array with objects
 * @param {Boolean|Number|String} valueToFind is the value you're trying to find in the array
 * @param {String} objectKey at which property of the object should the function look.
 */
export const getArrayIndexByObjectValue = (arrayToSearch, valueToFind, objectKey) => {
  let data = arrayToSearch.map((item, index) => {
    if (item[objectKey] === valueToFind) {
      return index;
    }
    return false;
  });

  data = data.filter(item => typeof item === 'number');

  return data[0];
};

/**
 * Returns a formated date based using moment js
 * @param {String|Number} date is the date itself, it should be either a date int or a date string like '1990-22-11'
 * @param {String} format is the return output format type
 *                for more format Types look here https://momentjs.com/docs/#/displaying/format/
 */
export const getFormatedDate = (date = Date.now(), format = 'MMMM YYYY') =>
  moment(date).format(format);
