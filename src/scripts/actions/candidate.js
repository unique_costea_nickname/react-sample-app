import { apiClient } from 'yc-api-client';
import { parseCandidateParams } from '../services/parser';
import { addNotification } from './notification';

import {
  REQUEST_CANDIDATE_INFO,
  RECEIVE_CANDIDATE_INFO,
  RECV_ERROR,
  FETCH_ADDRESS_INFO,
  RECEIVED_ADDRESS,
  ADDRESS_NOT_FOUND,
  RESUME_FILE_UPLOAD_FORM,
  RESUME_FORM,
} from '../stores/mutation-types';

function getCandidateUuidFromState(state) {
  const auth = {
    uuid: state.candidate.uuid,
    authToken: state.candidate.auth_token,
  };

  return auth;
}

function requestCandidateInfo(uuid) {
  return {
    type: REQUEST_CANDIDATE_INFO,
    payload: {
      uuid,
    },
  };
}

function receiveCandidateInfo(json) {
  return {
    type: RECEIVE_CANDIDATE_INFO,
    payload: {
      attributes: json.data.data.attributes,
      links: json.data.data.links,
      uuid: json.data.data.id,
      receivedAt: Date.now(),
    },
  };
}

function receiveError(json) {
  return {
    type: RECV_ERROR,
    data: json,
  };
}

function fetchCandidateInfo(uuid, authToken) {
  return (dispatch) => {
    dispatch(requestCandidateInfo(uuid));

    return apiClient.new(authToken).getCandidateData()
      .then((response) => {
        dispatch(receiveCandidateInfo(response));
      })
      .catch((err) => {
        dispatch(receiveError(err));
        dispatch(addNotification(err, 'error'));
      });
  };
}

function shouldFetchCandidateInfo(state) {
  return (!state.candidate.isLoaded && !state.candidate.isFetching);
}

export function fetchCandidateInfoIfNeeded() {
  return (dispatch, getState) => {
    const { uuid, authToken } = getCandidateUuidFromState(getState());

    if (shouldFetchCandidateInfo(getState())) {
      return dispatch(fetchCandidateInfo(uuid, authToken));
    }
    return Promise.resolve();
  };
}

export function updateCandidateInfo(formType) {
  return (dispatch, getState) => {
    const attributes = getState().form[formType.toString()].values;
    const { uuid, authToken } = getCandidateUuidFromState(getState());

    const parsedAttributes = parseCandidateParams(formType, attributes);

    apiClient.new(authToken).updateCandidateAttributes(uuid, parsedAttributes)
      .then((response) => {
        dispatch(addNotification('Updated profile', 'success'));
        dispatch(receiveCandidateInfo(response));
      })
      .catch((err) => {
        dispatch(receiveError(err));
        dispatch(addNotification(`Couldnt update your profile: ${err}`, 'error'));
      });
  };
}

export function uploadFile(formType) {
  return (dispatch, getState) => {
    const formTypeForParser = (formType === RESUME_FORM ? RESUME_FILE_UPLOAD_FORM : formType);
    const attributes = getState().form[formType.toString()].values;

    const parsedAttributes = parseCandidateParams(formTypeForParser, attributes);
    const { uuid, authToken } = getCandidateUuidFromState(getState());

    apiClient.new(authToken).uploadCandidateFile(parsedAttributes)
      .then((response) => {
        if (response) {
          dispatch(fetchCandidateInfo(uuid, authToken));
        }
      })
      .catch((err) => {
        dispatch(receiveError(err));
        dispatch(addNotification(`Photo upload went wrong ${err}`, 'error'));
      });
  };
}

function receivedAddress(data) {
  if (data.length) {
    return {
      type: RECEIVED_ADDRESS,
      payload: {
        addressPresent: true,
        address: data[0].attributes,
      },
    };
  }

  return {
    type: ADDRESS_NOT_FOUND,
    payload: {
      addressPresent: false,
    },
  };
}

export function fetchAddress() {
  return (dispatch, getState) => {
    const { zipcode, number } = getState().form.BASIC_FORM.values.home_address;
    const { authToken } = getCandidateUuidFromState(getState());

    apiClient.new(authToken).searchZipcodeByZipcodeAndNumber(zipcode, number)
      .then((response) => {
        dispatch(receivedAddress(response.data.data));
      });

    return {
      type: FETCH_ADDRESS_INFO,
      payload: {
        addressPresent: false,
      },
    };
  };
}

export const cropImage = crop => (
  {
    type: 'IMAGE_CROPPED',
    payload: {
      crop,
    },
  }
);
