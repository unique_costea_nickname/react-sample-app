import { ADD_NOTIFICATION, REMOVE_LOADING_INDICATOR } from '../stores/mutation-types';

export function addNotification(message, level) {
  return {
    type: ADD_NOTIFICATION,
    message,
    level,
  };
}

export function addLoadingIndicator() {
  console.debug('addLoadingIndicator actions :: addNotification', '');
  return {
    type: REMOVE_LOADING_INDICATOR,
    loading: false,
  };
}
