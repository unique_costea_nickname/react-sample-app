export const toggleModal = (toggleStatus, payload) => {
  if (toggleStatus === 'show') {
    return {
      type: 'MODAL_SHOW',
      payload: {
        type: payload.type,
        visible: true,
      },
    };
  }

  return {
    type: 'MODAL_HIDE',
    payload: {
      visible: false,
    },
  };
};
