import { apiClient } from 'yc-api-client';
import store from '../stores/index';

export function searchEducationsByNameAndEduType(term, eduTypeId) {
  const authToken = store.getState().candidate.auth_token;

  return apiClient.new(authToken).searchEducationsByNameAndEduType(term, eduTypeId);
}
