import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Field, reduxForm, FieldArray } from 'redux-form';
import { DEGREE_FORM } from '../../stores/mutation-types';

import {
  EducationName,
  EduLevel,
  EduType,
  StartDate,
  EndDate,
} from '../form/render-educations';

import { updateCandidateInfo } from '../../actions/candidate';

import { addNotification } from '../../actions/notification';

import { renderSelect } from '../form/render-fields';
import settings from '../../reducers/data/education-settings';

class EducationModal extends Component {
  constructor(props) {
    super(props);

    this.state = { fieldsToShow: [] };

    this.determineFieldsToRenderBasedOnEduType = this.determineFieldsToRenderBasedOnEduType.bind(this);
    this.fieldArraysList = this.fieldArraysList.bind(this);
    this.eduTypeChanged = this.eduTypeChanged.bind(this);
  }

  componentDidMount() {
    this.initializeForm();
    addNotification('it went horribly wrong', 'error');
  }

  initializeForm() {
    const { degrees } = this.props;
    this.props.initialize({ degrees });

    degrees.map((item, index) => this.determineFieldsToRenderBasedOnEduType(index, item.edu_type_id));
  }

  renderRemoveButton(fields, index) {
    if (fields.length === 1) {
      return false;
    }

    return (
      <div className="degree__remove-block">
        <button type="button" onClick={() => fields.remove(index)}
          className="degree__remove-btn">Verwijder <i className="nyc-icon nyc-icon-close-cross degree__remove-btn-icon"></i></button>
      </div>
    );
  }

  renderAddButton(fields) {
    return (
      <div className="row base__block--spaced">
        <div className="col-xs-12">
          <div className="btn--dash">
            <hr />
            <button type="button" onClick={() => fields.push({})}
              className="btn btn--tertiary btn--outline btn--white">Voeg opleiding toe</button>
            <hr />
          </div>
        </div>
      </div>
    );
  }

  getComponentByType(type, attrs) {
    const componentTypes = {
      EduLevel,
      EducationName,
      EndDate,
      StartDate,
    };

    return componentTypes[type](attrs);
  }

  renderFieldsBasedOnEduType(item, index) {
    const { fieldsToShow } = this.state;

    const list = fieldsToShow[index] && fieldsToShow[index].list.map((name, i) => {
      const options = fieldsToShow[index].options[name] || [];

      const element = this.getComponentByType(name, {
        key: item + name,
        item,
        options,
      });

      return element;
    });
    return <div>{list}</div>;
  }

  determineFieldsToRenderBasedOnEduType(index, value) {
    const fields = this.state.fieldsToShow;
    const eduTypeSettings = settings.edu_types.filter(setting => setting.id === value)[0].options;
    const fieldsToAdd = [];
    const optionsToAdd = {};

    if (eduTypeSettings.require_edu_level) {
      fieldsToAdd.push('EduLevel');
      optionsToAdd.EduLevel = (value === 1 ? settings.edu_level.high_school : settings.edu_level.mbo);
    }
    if (eduTypeSettings.require_education) {
      fieldsToAdd.push('EducationName');
      optionsToAdd.EducationName = { eduTypeId: value };
    }

    if (eduTypeSettings.require_period) { fieldsToAdd.push('StartDate', 'EndDate'); }

    fields[index] = {
      list: fieldsToAdd,
      options: optionsToAdd,
    };

    this.setState({ fieldsToShow: fields });
  }

  eduTypeChanged(e, index, value) {
    this.determineFieldsToRenderBasedOnEduType(index, value);
    this.resetFieldValues(index);
  }

  resetFieldValues(index) {
    const names = [
      `degrees[${index}].education_name`,
      `degrees[${index}].edu_level_id`,
      `degrees[${index}].start_date`,
      `degrees[${index}].end_date`,
    ];

    names.map(name => this.props.change(name, ''));
  }

  fieldArraysList({ fields, meta: { touched, error } }) {
    const { degrees } = this.props;

    const eduTypeOptions = settings.edu_types.map((item) => {
      return { label: item.name, value: item.id };
    });

    return (
      <div>
        {touched && error && <span>{error}</span>}
        {fields.map((item, index) =>
          <div className="popup__box" key={ index }>
            { this.renderRemoveButton(fields, index) }
            <div className="row middle-xs input form__input">
              <div className="col-xs-12 col-sm-4">
                <div className="form__label">Type</div>
              </div>

              <div className="col-xs-12 col-sm-8">
                <Field name={`${item}.edu_type_id`}
                  key={index}
                  component={ renderSelect }
                  options={ eduTypeOptions }
                  custom={{ clearable: false, searchable: false, options: eduTypeOptions }}
                  onChange={(event, value) => this.eduTypeChanged.apply(this, [event, index, value])}
                />
              </div>
            </div>
            { this.renderFieldsBasedOnEduType.apply(this, [item, index]) }
          </div>,
        )}
        { this.renderAddButton(fields) }
      </div>
    );
  }

  render() {
    const { handleSubmit } = this.props;

    return (
      <div className="col-xs">
        <form className="form" onSubmit={ handleSubmit(this.props.onSubmitAction.bind(this, DEGREE_FORM)) }>
          <div className="row">
            <div className="col-xs-12 col-sm-8">
              <FieldArray name="degrees" component={ this.fieldArraysList } rerenderOnEveryChange={true} />
            </div>
          </div>
          <div className="row save-button">
            <div className="col-xs-12 col-sm-8 col-sm-offset-4 col-md-9 col-md-offset-3 base__block--spaced">
              <div className="row end-xs">
                <div className="col-xs-12 col-sm-6 col-md-5">
                  <button className="btn btn--primary btn--wide base__btn" type="submit">gegevens opslaan</button>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    );
  }
}

const mapStateToProps = state => (
  {
    degrees: state.candidate.attributes.degrees,
  }
);

function mapDispatchToProps(dispatch) {
  return {
    onSubmitAction: formType => dispatch(updateCandidateInfo(formType)),
  };
}

EducationModal.propTypes = {
  degrees: PropTypes.array.isRequired,
  initialize: PropTypes.func,
  handleSubmit: PropTypes.func.isRequired,
  onSubmitAction: PropTypes.func.isRequired,
};


export default reduxForm({
  form: DEGREE_FORM,
  enableReinitialize: true,
})(connect(mapStateToProps, mapDispatchToProps)(EducationModal));
