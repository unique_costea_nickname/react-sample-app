import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { reduxForm, Field, formValueSelector } from 'redux-form';

import { RenderCheckboxList, renderRadioGroup } from '../form/render-fields';
import { EXTRA_FORM } from '../../stores/mutation-types';
import { updateCandidateInfo } from '../../actions/candidate';
import languagesList from '../../reducers/data/languages-list';

class ExtraModal extends Component {
  constructor(props) {
    super(props);

    this.state = {
      checkboxes: languagesList,
      radioList: [
        {
          value: true,
          label: 'Ja',
        },
        {
          value: false,
          label: 'Nee',
        },
      ],
    };
  }

  componentDidUpdate() {
    if (!this.props.hasDrivingLicense) {
      this.props.change('has_car', false);
    }
  }

  componentDidMount() {
    this.initializeForm();
  }

  initializeForm() {
    const { extraAttributes } = this.props;

    this.props.initialize(extraAttributes);
  }

  hasCarQuestion() {
    return (
      <div className="popup__box popup__box--no-line">
        <div className="row">
          <div className="col-xs-12 form__label">
            Do you own a car?
          </div>
          <div className="col-xs-12">
            <Field
              options={this.state.radioList}
              attributes={ this.props.extraAttributes }
              className="base__block--spaced-top base--inline"
              childrenClassList="base__block"
              component={ renderRadioGroup }
              name="has_car"
            />
          </div>
        </div>
      </div>
    );
  }

  render() {
    const { extraAttributes, handleSubmit, hasDrivingLicense } = this.props;

    return (
      <div className="col-xs-12 col-sm-8 col-md-9">
        <form className="form" onSubmit={ handleSubmit(this.props.onSubmitAction.bind(this, EXTRA_FORM)) }>
          <div className="popup__box base__block--sd-bottom">
            <div className="col-sm-10 col-md-8">
              <div className="row">
                <div className="col-xs-12 form__label">
                  Welke talen beheers je?
                </div>
                <div className="col-xs-12">
                  <Field
                    options={ this.state.checkboxes }
                    selectedLanguages={ extraAttributes.candidate_languages }
                    checkboxType="square"
                    className="row base__block--spaced-top"
                    childrenClassList="col-sm-4 col-xs-6 col-md-3"
                    component={ RenderCheckboxList }
                    name="candidate_languages"
                    limit={4}
                    showMoreText="meer"
                  />
                </div>
              </div>
            </div>
          </div>

          <div className="popup__box popup__box--no-line popup__box--driving">
            <div className="row">
              <div className="col-xs-12 form__label">
                Ben je in het bezit van een rijbewijs?
              </div>
              <div className="col-xs-12">
                <Field
                  options={ this.state.radioList }
                  attributes={ extraAttributes }
                  className="base__block--spaced-top base--inline"
                  childrenClassList="base__block"
                  component={ renderRadioGroup }
                  name="has_license"
                />
              </div>
            </div>
          </div>
          { hasDrivingLicense && this.hasCarQuestion() }

          <div className="row save-button">
            <div className="col-xs-12 col-sm-8 col-sm-offset-4 col-md-9 col-md-offset-3 base__block--spaced">
              <div className="row end-xs">
                <div className="col-xs-12 col-sm-6 col-md-5">
                  <button className="btn btn--primary btn--wide" type="submit">Opslaan</button>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return {
    onSubmitAction: formType => dispatch(updateCandidateInfo(formType)),
  };
}

ExtraModal.propTypes = {
  extraAttributes: PropTypes.object.isRequired,
  isLoaded: PropTypes.bool,
  initialize: PropTypes.func,
  onSubmitAction: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  hasDrivingLicense: PropTypes.bool,
};

const selector = formValueSelector(EXTRA_FORM);

export default reduxForm({
  form: EXTRA_FORM,
})(connect((state) => {
  const hasDrivingLicense = selector(state, 'has_license');

  return {
    hasDrivingLicense,
    extraAttributes: state.candidate.attributes,
  };
}, mapDispatchToProps)(ExtraModal));
