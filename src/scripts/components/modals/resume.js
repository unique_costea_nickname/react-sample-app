import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { reduxForm, Field, formValueSelector } from 'redux-form';
import { connect } from 'react-redux';

import { updateCandidateInfo, uploadFile } from '../../actions/candidate';
import { RESUME_FORM } from '../../stores/mutation-types';
import { renderField } from '../form/render-fields';

import ResumeUploadField from '../form/resume-uploader';
import WorkExperience from '../form/workexperience';

class ResumeModal extends Component {
  constructor() {
    super();

    this.state = {
      types: {
        workex: { visible: false, component: false },
        linkedin: { visible: false, component: false },
        resumeupload: { visible: false, component: false },
      },
    };
    this.onClick = this.onClick.bind(this);
    this.getComponentByType = this.getComponentByType.bind(this);
    this.determineVisibleAndComponents = this.determineVisibleAndComponents.bind(this);
  }

  componentDidMount() {
    this.initializeForm();
  }

  componentWillReceiveProps(nextProps) {
    this.determineVisibleAndComponents(nextProps);
  }

  determineVisibleAndComponents(props = this.props) {
    const { resumeAttributes } = props;
    const { types } = this.state;

    if (resumeAttributes.work_experiences.length !== 0) {
      types.workex = {
        visible: true,
        component: this.getComponentByType('workex', props),
      };
    }

    if (resumeAttributes.linkedin_link !== null) {
      types.linkedin = {
        visible: true,
        component: this.getComponentByType('linkedin', props),
      };
    }

    if (resumeAttributes.resume_present) {
      types.resumeupload = {
        visible: true,
        component: this.getComponentByType('resumeupload', props),
      };
    }

    this.setState({ types });
  }

  initializeForm() {
    const { resumeAttributes } = this.props;

    this.props.initialize(resumeAttributes);
  }

  onClick(type) {
    const { types } = this.state;

    if (type === 'workex' && !types[type].visible) {
      this.props.initialize({ work_experiences: [{}] });
    }
    if (!types[type].visible) {
      types[type] = {
        visible: true,
        component: this.getComponentByType(type),
      };
    } else {
      types[type] = {
        visible: false,
        component: false,
      };
    }

    this.setState({ types });
  }

  getComponentByType(type, newProps = this.props) {
    const { resumeAttributes, workExperiences } = newProps;

    switch (type) {
      case 'resumeupload':
        return <ResumeUploadField className="resume__upload-field" resumeAttributes={resumeAttributes} />;
      case 'linkedin':
        return <Field
          component={renderField}
          label='Link to profiel'
          name='linkedin_link'
          attributes={resumeAttributes}
        />;
      case 'workex':
        return <WorkExperience
          resumeAttributes={resumeAttributes}
          work_experiences={ workExperiences }
        />;
      default:
        return '';
    }
  }

  handleResumeModalSubmit() {
    const { types } = this.state;
    if (types.resumeupload.visible) { this.props.fileUploading(RESUME_FORM); }

    this.props.onSubmitAction(RESUME_FORM);
  }

  render() {
    const { types } = this.state;
    const { handleSubmit, pristine, submitting } = this.props;

    return (
      <div className="col-xs">
        <form className="form" onSubmit={ handleSubmit(this.handleResumeModalSubmit.bind(this)) }>
          <div className="row">
            <div className="col-xs-12 col-sm-8">

              <div className="popup__box">
                <div className="row top-xs input form__input">
                  <div className="col-xs-10">
                    <div className="form__label">I wil mijn CV uploaden (PDF, Word of RTF bestand)</div>
                  </div>
                  <div className="col-xs-2">
                    <div className="input">
                      <input className="input__switch"
                        id="input_checkbox_1"
                        type="checkbox"
                        onClick={() => this.onClick('resumeupload')}
                        checked={types.resumeupload.visible}
                      />
                      <label className="input__switch-btn" htmlFor="input_checkbox_1"/>
                    </div>
                  </div>
                </div>
                <div>
                  {types.resumeupload.component}
                </div>
              </div>

              <div className="popup__box popup__box--line">
                <div className="row top-xs input form__input">
                  <div className="col-xs-10 col-sm-10">
                    <div className="form__label">Ik wil mijn LinkedIn profiel toevoegen</div>
                  </div>
                  <div className="col-xs-2 col-sm-2">
                    <div className="input">
                      <input className="input__switch"
                        id="input_checkbox_2"
                        type="checkbox"
                        onClick={() => this.onClick('linkedin')}
                        checked={types.linkedin.visible}
                      />
                      <label className="input__switch-btn" htmlFor="input_checkbox_2" />
                    </div>
                  </div>
                </div>
                <div>
                  { types.linkedin.component }
                </div>
              </div>

              <div className="popup__box popup__box--no-line">
                <div className="row top-xs input form__input">
                  <div className="col-xs-10 col-sm-10">
                    <div className="form__label">Ik wil mijn werkervaring handmatig invoeren</div>
                  </div>
                  <div className="col-xs-2 col-sm-2">
                    <div className="input">
                      <input className="input__switch"
                        id="input_checkbox_3"
                        type="checkbox"
                        onClick={() => this.onClick('workex')}
                        checked={types.workex.visible}
                      />
                      <label className="input__switch-btn" htmlFor="input_checkbox_3" />
                    </div>
                  </div>
                </div>
                <div>
                  { types.workex.visible && this.getComponentByType('workex', {
                    resumeAttributes: this.props.resumeAttributes,
                    workExperiences: this.props.workExperiences,
                  })}
                </div>
              </div>
            </div>
          </div>
          <div className="row save-button">
            <div className="col-xs-12 col-sm-8 col-sm-offset-4 col-md-9 col-md-offset-3 base__block--spaced">
              <div className="row end-xs">
                <div className="col-xs-12 col-sm-6 col-md-5">
                  <button disabled={ pristine || submitting }
                    className="btn btn--primary btn--wide"
                    type="submit">gegevens opslaan
                  </button>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return {
    fileUploading: formType => dispatch(uploadFile(formType)),
    onSubmitAction: formType => dispatch(updateCandidateInfo(formType)),
  };
}

ResumeModal.propTypes = {
  resumeAttributes: PropTypes.object.isRequired,
  workExperiences: PropTypes.array,
  initialize: PropTypes.func,
  handleSubmit: PropTypes.func.isRequired,
  pristine: PropTypes.func,
  submitting: PropTypes.func,
  onSubmitAction: PropTypes.func,
  fileUploading: PropTypes.func,
};

const selector = formValueSelector(RESUME_FORM);
export default reduxForm({
  form: RESUME_FORM,
  enableReinitialize: true,
})(connect((state) => {
  const workExperiences = selector(state, 'work_experiences');

  return {
    workExperiences,
    resumeAttributes: state.candidate.attributes.resume,
  };
}, mapDispatchToProps)(ResumeModal));
