import React, { Component } from 'react';
import { reduxForm, Field } from 'redux-form';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import PhotoEditorField from '../form/render-photo-editor';
import { renderInputFile } from '../form/render-fields';
import { PHOTO_UPLOAD } from '../../stores/mutation-types';
import { uploadFile, cropImage } from '../../actions/candidate';

import candidateFemale from '../../../assets/candidate_female.png';
import candidateMale from '../../../assets/candidate_male.png';

class PhotoEditorForm extends Component {
  renderCropper() {
    const { imageAttributes, onImageCrop } = this.props;
    if (imageAttributes.picture_present) {
      return <Field
        className="photo-editor__field"
        name="photo-cropper"
        component={PhotoEditorField}
        src={imageAttributes.imageUrl}
        onCropCompleted={onImageCrop}
        aspectRatio={5 / 6}
        style={{
          width: '100%',
          height: 'auto',
          display: 'block',
        }}
        crop={imageAttributes.crop}
      />;
    }
    return (
      <div className="photo-editor__cropper-placeholder photo-editor__field">
        <div className="photo-editor__copper-placeholder-container text--center color--grey-tertiary">
          <i className="nyc-icon nyc-icon-cropper nyc-icon-3x"></i>
          <div>Na het uploaden kan je hier je foto bijsnijden.</div>
        </div>
      </div>
    );
  }

  render() {
    const { handleSubmit, imageAttributes, isMale } = this.props;
    let inputField = null;

    let candidatePicture = null;
    if (imageAttributes.picture_present) {
      candidatePicture = imageAttributes.imageUrl;
    } else {
      candidatePicture = candidateFemale;
      if (isMale) candidatePicture = candidateMale;
    }

    const openFileInput = () => {
      inputField.click();
    };

    return (
      <div className="col-xs-12 col-sm-8 col-md-9">
        <form className="form" onSubmit={ handleSubmit(this.props.fileUploading.bind(this, PHOTO_UPLOAD)) }>
          <div className="popup__box">
            <div className="col-sm-10 col-md-8">
              <div className="row photo-editor">
                <div className="col-xs-12 col-sm-4">
                  <div className="photo-editor__preview">
                    <img className="photo-editor__image" src={candidatePicture} alt="Preview of Crop" />
                  </div>
                  <div className="photo-editor__actions">
                    <div className="hidden">
                      <Field
                        name="photo"
                        component={renderInputFile}
                        accept=".png,.gif,.jpeg,.pjpeg',.x-png"
                        inputRef={(inputFile) => { inputField = inputFile; }}
                        withRef={true}
                      />
                    </div>
                    <button className="btn btn--secondary btn--wide base__block--spaced" type="button" onClick={ openFileInput } >vervangen</button>
                    <button className="btn btn--tertiary btn--outline btn--wide" type="button">verwijderen</button>
                  </div>
                </div>
                <div className="col-xs-12 col-sm-8">
                  { this.renderCropper() }
                </div>
              </div>
            </div>
          </div>
          <div className="row save-button">
            <div className="col-xs-12 col-sm-8 col-sm-offset-4 col-md-9 col-md-offset-3 base__block--spaced">
              <div className="row end-xs">
                <div className="col-xs-12 col-sm-6 col-md-5">
                  <button className="btn btn--primary btn--wide" type="submit">opslaan</button>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    );
  }
}
const mapStateToProps = state => (
  {
    imageAttributes: state.candidate.attributes.picture,
    isMale: state.candidate.attributes.is_male,
  }
);

function mapDispatchToProps(dispatch) {
  return {
    fileUploading: formType => dispatch(uploadFile(formType)),
    onImageCrop: crop => dispatch(cropImage(crop)),
  };
}

PhotoEditorForm.propTypes = {
  imageAttributes: PropTypes.object.isRequired,
  onImageCrop: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  fileUploading: PropTypes.func,
  isMale: PropTypes.bool.isRequired,
};

export default reduxForm({
  form: PHOTO_UPLOAD,
})(connect(mapStateToProps, mapDispatchToProps)(PhotoEditorForm));
