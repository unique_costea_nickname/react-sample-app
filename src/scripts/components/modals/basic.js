import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { reduxForm, Field } from 'redux-form';
import { countriesList } from 'yc-api-client';
import { connect } from 'react-redux';
import {
  fetchAddress,
  updateCandidateInfo,
} from '../../actions/candidate';

import { renderSelect } from '../form/render-fields';
import { BASIC_FORM } from '../../stores/mutation-types';
import DatePicker from '../form/date-picker';

class BasicModal extends Component {
  constructor(props) {
    super(props);

    this.reduxFormOnChange = this.props.change;
  }

  componentDidMount() {
    this.initializeForm();
  }

  componentWillReceiveProps(nextProps) {
    this.reduxFormOnChange('street', nextProps.attributes.home_address.street);
    this.reduxFormOnChange('city', nextProps.attributes.home_address.city);
  }

  initializeForm() {
    const { attributes } = this.props;

    this.props.initialize(attributes);
  }

  showAdditionalAddressFields() {
    const { home_address } = this.props.attributes;

    // We dont need to render the AdditionalFields if the address is found.
    if (home_address.addressPresent) { return false; }

    const defaultLabelColClass = 'col-xs-12 col-sm-4';
    const defaultContentColClass = 'col-xs-12 col-sm-8';

    return (
      <div className="addtional-address-fields">
        <div className="row middle-xs input form__input">
          <div className={defaultLabelColClass}>
            <div className="form__label">Straat</div>
          </div>
          <div className={defaultContentColClass}>
            <Field
              name="home_address.street"
              className="input__text" component="input"
              type="text"
              value={ home_address.street }
            />
          </div>
        </div>

        <div className="row middle-xs input form__input">
          <div className={defaultLabelColClass}>
            <div className="form__label">Plaats</div>
          </div>

          <div className={defaultContentColClass}>
            <Field
              name="home_address.city"
              className="input__text"
              component="input"
              type="text"
              value={ home_address.city }
            />
          </div>
        </div>
      </div>
    );
  }

  showAutocompletedAddress() {
    const { home_address } = this.props.attributes;
    let icon,
      message;

    if (home_address.addressPresent) {
      icon = <i className="nyc-icon nyc-icon-toast-check card__icon-check color--notifications-brand"></i>;
      message = `${home_address.street} ${home_address.number}, ${home_address.zipcode}, ${home_address.city}`;
    } else {
      icon = <i className="nyc-icon nyc-icon-toast-error card__icon-check color--notifications-error"></i>;
      message = 'Het adres is niet gevonden';
    }

    return (
      <div className="row middle-xs">
        <div className="col-xs-12 col-sm-8 col-sm-offset-4">
          <p className="text--italic text--validation">{icon} {message}</p>
        </div>
      </div>
    );
  }

  render() {
    const { handleSubmit, attributes } = this.props;
    const defaultLabelColClass = 'col-xs-12 col-sm-4';
    const defaultContentColClass = 'col-xs-12 col-sm-8';
    const genderOptions = [
      {
        value: true,
        label: 'Man',
      },
      {
        value: false,
        label: 'Vrouw',
      },
    ];

    return (
      <div className="col-xs-12 col-sm-8 col-md-9">
        <form className="form" onSubmit={ handleSubmit(this.props.onSubmitAction.bind(this, BASIC_FORM)) }>
          <div className="popup__box">
            <div className="col-sm-10 col-md-8">
              <div className="row middle-xs input form__input">
                <div className={ defaultLabelColClass }>
                  <div className="form__label">Voornaam</div>
                </div>

                <div className={defaultContentColClass}>
                  <Field
                    name="firstname"
                    component="input"
                    className="input__text"
                    type="text"
                    value={ attributes.firstname }
                  />
                </div>
              </div>

              <div className="row middle-xs input form__input">
                <div className={ defaultLabelColClass }>
                  <div className="form__label">Achternaam</div>
                </div>

                <div className={defaultContentColClass}>
                  <Field
                    name="lastname"
                    component="input"
                    className="input__text" type="text"
                    value={ attributes.lastname }
                  />
                </div>
              </div>

              <div className="row middle-xs input form__input">
                <div className={defaultLabelColClass}>
                  <div className="form__label">Geslacht</div>
                </div>

                <div className={defaultContentColClass}>
                  <Field
                    name="is_male"
                    custom={{ clearable: false, searchable: false, options: genderOptions }}
                    component={renderSelect}
                  />
                </div>
              </div>

              <div className="row middle-xs input form__input">
                <div className={defaultLabelColClass}>
                  <div className="form__label">Geboortedatum</div>
                </div>

                <div className={defaultContentColClass}>
                  <Field name="date_of_birth" component={DatePicker}/>
                </div>
              </div>

              <div className="row middle-xs input form__input">
                <div className={ defaultLabelColClass }>
                  <div className="form__label">Email</div>
                </div>

                <div className={defaultContentColClass}>
                  <Field
                    name="email"
                    className="input__text"
                    component="input"
                    type="email"
                    value={ attributes.email }
                  />
                </div>
              </div>

              <div className="row middle-xs input form__input">
                <div className={ defaultLabelColClass }>
                  <div className="form__label">Mobiel / Telefoon</div>
                </div>

                <div className={defaultContentColClass}>
                  <Field
                    name="telephone"
                    className="input__text"
                    component="input"
                    type="tel"
                    value={ attributes.telephone }
                  />
                </div>
              </div>

              <div className="row middle-xs input form__input">
                <div className={defaultLabelColClass}>
                  <div className="form__label">Land</div>
                </div>


                <div className={defaultContentColClass}>
                  <Field
                    name="home_address.country_id"
                    custom={{ searchable: true, options: countriesList }}
                    value={ attributes.home_address.country_id }
                    component={ renderSelect }
                  />
                </div>
              </div>

              <div className="row middle-xs input form__input">
                <div className={ defaultLabelColClass }>
                  <div className="form__label">Postcode</div>
                </div>

                <div className={ defaultContentColClass }>
                  <div className="row">
                    <div className="col-xs">
                      <Field
                        type="text"
                        name="home_address.zipcode"
                        className="input__text"
                        component="input"
                        value={ attributes.home_address.zipcode }
                        onBlur={ () => this.props.fetchAddress() }
                      />
                    </div>
                    <div className="col-xs">
                      <Field
                        type="text"
                        name="home_address.number"
                        className="input__text"
                        component="input"
                        value={ attributes.home_address.number }
                        onBlur={ () => this.props.fetchAddress() }
                      />
                    </div>
                  </div>
                </div>
              </div>

              { this.showAdditionalAddressFields() }
              { this.showAutocompletedAddress() }
            </div>
          </div>
          <div className="row save-button">
            <div className="col-xs-12 col-sm-8 col-sm-offset-4 col-md-9 col-md-offset-3 base__block--spaced">
              <div className="row end-xs">
                <div className="col-xs-12 col-sm-6 col-md-5">
                  <button className="btn btn--primary btn--wide" type="submit">gegevens opslaan</button>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    attributes: state.candidate.attributes,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    fetchAddress: () => dispatch(fetchAddress()),
    onSubmitAction: formType => dispatch(updateCandidateInfo(formType)),
  };
}

BasicModal.propTypes = {
  attributes: PropTypes.object.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  initialize: PropTypes.func,
  change: PropTypes.func,
};

export default reduxForm({
  form: BASIC_FORM,
})(connect(mapStateToProps, mapDispatchToProps)(BasicModal));
