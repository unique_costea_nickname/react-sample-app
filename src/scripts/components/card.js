import React from 'react';
import PropTypes from 'prop-types';

import candidateFemale from '../../assets/candidate_female.png';
import candidateMale from '../../assets/candidate_male.png';

const Card = (props) => {
  let rows;

  const handleOpenModal = (e) => {
    let type = props.data.modalType;

    if (e && e.nativeEvent.target.attributes.href) {
      e.nativeEvent.preventDefault();
      type = e.nativeEvent.target.attributes.href.value;
    }
    props.openModal('show', { type });
  };

  const createRows = () => {
    rows = (
      props.data.keyList.map((item) => {
        const { label } = item;
        let value = props.attributes[item.key];

        // Custom Label and content
        if (item.key === 'profile_picture') {
          const { attributes } = props;
          const picturePresent = attributes.picture.picture_present;
          let candidatePictureUrl;

          if (picturePresent) {
            candidatePictureUrl = attributes.picture.imageUrl;
          } else if (!picturePresent && attributes.is_male === true) {
            candidatePictureUrl = candidateMale;
          } else if (!picturePresent && attributes.is_male === false) {
            candidatePictureUrl = candidateFemale;
          } else {
            candidatePictureUrl = null;
          }

          if (candidatePictureUrl) {
            return (
              <div className="row card__row" key={item.key}>
                <div className="col-xs-12">
                  <figure className="card__figure">
                    <img
                      className="card__image"
                      src={candidatePictureUrl}
                      alt={`${props.attributes.firstname} ${props.attributes.lastname}`}
                    />
                  </figure>
                </div>
              </div>
            );
          }

          return null;
        } else if (item.key === 'is_male') {
          value = props.attributes[item.key] ? 'Man' : 'Vrouw';
        } else if (item.key === 'home_address') {
          if (props.attributes.home_address.addressPresent) {
            value = `${props.attributes.home_address.street} ${props.attributes.home_address.number}, ${props.attributes.home_address.zipcode},
              ${props.attributes.home_address.city}`;
          } else {
            value = null;
          }
        } else if (item.key === 'resume_title') {
          if (props.attributes.resume_present) {
            value = <a href={ props.attributes.resume } className="base__link" download>{ 'Download uploaded CV' }</a>;
          }
        }

        if (!value) { value = <span className="card__n-a">(Niet toegevoegd)</span>; }

        return (
          <div className="card__row row" key={item.key}>
            <div className="form__label col-sm-4 col-xs-12">
              {label}
            </div>
            <div className="form__text col-sm-8 col-xs-12">
              { value }
            </div>
          </div>
        );
      })
    );

    return rows;
  };

  const showTitle = () => {
    if (props.data && props.data.title) {
      return (
        <div className="col-xs-12 col-sm-4 col-md-3 col-lg-2">
          <h3 className="title">{ props.data.title }</h3>
        </div>
      );
    }
    return null;
  };

  const getContent = () => {
    if (props.children) {
      return props.children;
    }
    return (
      <div className="card__cols">
        {createRows()}
      </div>
    );
  };

  // Render Function
  return (
    <div className="row candidate-details__group">
      {showTitle()}
      <div className="col-xs-12 col-sm-8 col-sm-offset-0 col-md-6 col-md-offset-1 col-lg-6 col-lg-offset-1">
        <div className="card form candidate-details__card">
          <div className="form__edit">
            <button onClick={ handleOpenModal } className="form__btn-edit">
              <i className="nyc-icon nyc-icon-edit"></i>
            </button>
            {getContent()}
          </div>
        </div>
      </div>
    </div>
  );
};

Card.propTypes = {
  data: PropTypes.shape({
    title: PropTypes.string,
    keyList: PropTypes.array,
    modalType: PropTypes.string,
  }),
  attributes: PropTypes.object.isRequired,
  children: PropTypes.element,
  openModal: PropTypes.func,
};

export default Card;
