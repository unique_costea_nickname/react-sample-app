import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Header from './header';
import Navigation from './navigation';
import Dashboard from './dashboard';
import PersonalInfo from '../containers/personal-info';
import Settings from './settings';

class Page extends Component {
  getComponentByType(type) {
    const componentTypes = {
      PersonalInfo,
      Dashboard,
      Settings,
    };

    return componentTypes[type];
  }

  render() {
    const pageName = this.props.pageName || this.props.match.params.pageName;
    const pageData = this.props.page[pageName];

    const PageComponent = this.getComponentByType(pageData.componentName);
    return (
      <div>
        <div className="wrap bg--grey-light">
          <Navigation />
          <Header header={ pageData.header } />
          <PageComponent />
        </div>
      </div>
    );
  }
}

Page.propTypes = {
  page: PropTypes.object.isRequired,
  pageName: PropTypes.string,
};

export default Page;
