import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { getFormatedDate } from '../../helpers';

import settings from '../../reducers/data/education-settings';

class EducationCard extends Component {
  constructor(props) {
    super(props);

    this.state = {
      results: settings,
    };
  }

  showEducationType(item) {
    const eduTypes = settings.edu_types;
    const result = eduTypes.filter(eduType => eduType.id === item.edu_type_id)[0];

    if (result) {
      return <div className="card__row">{result.name}</div>;
    }

    return false;
  }

  showEducationLevel(item) {
    const allEduLevels = Object.values(settings.edu_level)[0].concat(Object.values(settings.edu_level)[1]);
    const result = allEduLevels.filter(level => level.value === item.edu_level_id)[0];

    if (result) {
      return <div className="card__row">{result.label}</div>;
    }

    return false;
  }

  showEducationGroup(item) {
    const result = item.edu_group != null ? item.edu_group : false;

    if (result) {
      return <div className="card__row">{result}</div>;
    }

    return false;
  }

  showEducationName(item) {
    const result = item.education_name != null ? item.education_name : false;

    if (result) {
      return <div className="card__row">{result}</div>;
    }

    return false;
  }

  showEducationDates(item) {
    let result;
    if (item.start_date != null) {
      result = `${getFormatedDate(item.start_date)} - ${getFormatedDate(item.end_date)}`;
    } else if (item.start_date != null && item.end_date === null) {
      result = `${getFormatedDate(item.start_date)}`;
    } else if (item.start_date === null && item.end_date != null) {
      result = `${getFormatedDate(item.end_date)}`;
    }

    if (result) {
      return <div className="card__row text--capitalize">{result}</div>;
    }

    return false;
  }

  render() {
    return (
      <div className="card__row card--separator row">
        <div className="form__label col-sm-4 col-xs-12">
          { this.showEducationType(this.props.degree) }
        </div>
        <div className="form__text col-sm-8 col-xs-12">
          { this.showEducationName(this.props.degree) }
          { this.showEducationLevel(this.props.degree) }
          { this.showEducationGroup(this.props.degree) }
          { this.showEducationDates(this.props.degree) }
        </div>
      </div>
    );
  }
}

EducationCard.propTypes = {
  edu_type: PropTypes.string,
  edu_group: PropTypes.string,
  name: PropTypes.string,
  degree: PropTypes.object.isRequired,
};

export default EducationCard;
