import React from 'react';
import PropTypes from 'prop-types';

const Extra = (props) => {
  const languages = props.attributes.candidate_languages;

  return (
    <div>
      <div className="card__row card__row--tags row">
        <div className="col-xs-12">
          <h3 className="title period--none card__title">Talen</h3>
          <div className="form--tags">
            { languages.map((language, key) => <span key={key} className="form__tag">{language.name}</span>) }
          </div>
        </div>
      </div>

      <div className="card__row card__row--tags row">
        <div className="col-xs-12">
          <h3 className="title period--none card__title">Rijbewijs</h3>
          <div className="form--tags">
            <span className="form__tag">{props.attributes.has_license ? 'Ja' : 'Nee'}</span>
          </div>
        </div>
      </div>

      { props.attributes.has_car && <div className="card__row card__row--tags row">
        <div className="col-xs-12">
          <h3 className="title period--none card__title">Auto</h3>
          <div className="form--tags">
            <span className="form__tag">{props.attributes.has_car ? 'Ja' : 'Nee'}</span>
          </div>
        </div>
      </div>
      }
    </div>
  );
};

Extra.propTypes = {
  initialize: PropTypes.func,
  attributes: PropTypes.shape({
    candidate_languages: PropTypes.array.isRequired,
    has_license: PropTypes.bool.isRequired,
    has_car: PropTypes.bool,
  }),
};

export default Extra;
