import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { fetchCandidateInfoIfNeeded } from '../actions/candidate';

class Dashboard extends Component {
  constructor(props) {
    super(props);

    if (!props.isFetching && !props.isLoaded) {
      props.fetchCandidate();
    }
  }

  render() {
    return (
      <div>
        <p>Profile Home Page</p>
      </div>
    );
  }
}

Dashboard.propTypes = {
  fetchCandidate: PropTypes.func.isRequired,
  isFetching: PropTypes.bool,
  isLoaded: PropTypes.bool,
};

function mapStateToProps(state) {
  return {
    candidate: state.candidate,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    fetchCandidate: () => dispatch(fetchCandidateInfoIfNeeded()),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);

