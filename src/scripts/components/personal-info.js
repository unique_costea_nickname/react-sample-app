import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Card from './card';
import PopupModal from './modal';
import List from './list';
import EducationCard from './cards/education';
import Extra from './cards/extra';

import { addNotification } from '../actions/notification';


export default class PersonalInfo extends Component {
  componentDidMount() {
    if (!this.props.isFetching && !this.props.isLoaded) {
      this.props.fetchCandidate();
    }

    addNotification('err', 'error');
  }

  render() {
    const { candidate, cards, toggleModal } = this.props;

    return (
      <section className="candidate-details">
        <div className="container container-fluid">
          <Card data={ cards.photo }
            attributes={ candidate.attributes }
            openModal={ (action, payload) => toggleModal(action, payload) }
          />
          <Card data={ cards.basic }
            attributes={ candidate.attributes }
            openModal={ (action, payload) => toggleModal(action, payload) }
          />
          <List
            data={ cards.education }
            items={ candidate.attributes.degrees }
            propName="degree"
            openModal={ (action, payload) => toggleModal(action, payload) }
            component={ EducationCard }
            limit={2}
            showAllText="Laat alles zien"
          />
          <Card data={ cards.resume }
            attributes={ candidate.attributes.resume }
            openModal={ (action, payload) => toggleModal(action, payload) }
          />
          <Card data={ cards.extra }
            attributes={ candidate }
            openModal={ (action, payload) => toggleModal(action, payload) }
          >
            <Extra attributes={ candidate.attributes } />
          </Card>
        </div>

        <PopupModal attributes={ candidate.attributes } />
      </section>
    );
  }
}

PersonalInfo.propTypes = {
  cards: PropTypes.object,
  candidate: PropTypes.object.isRequired,
  fetchCandidate: PropTypes.func.isRequired,
  isFetching: PropTypes.bool,
  isLoaded: PropTypes.bool,
  toggleModal: PropTypes.func,
};
