import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Header extends Component {
  showSubtitle() {
    return (
      <h2 className="subtitle--box">
        <span>
          {this.props.header.subTitle}
        </span>
      </h2>
    );
  }
  render() {
    return (
      <section className="header">
        <div className="container container-fluid">
          <div className="row">
            <div className="col-xs">
              <h1 className="title--box">
                <span>
                  {this.props.header.title}
                </span>
              </h1>
              <br />
              {this.showSubtitle()}
            </div>
          </div>
        </div>
      </section>
    );
  }
}


Header.propTypes = {
  header: PropTypes.shape({
    title: PropTypes.string.isRequired,
    subTitle: PropTypes.string,
  }),
};

export default Header;
