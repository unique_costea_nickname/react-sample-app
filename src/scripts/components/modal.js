import React, { Component } from 'react';
import ReactModal from 'react-modal';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { toggleModal } from '../actions/modal';
import { addNotification } from '../actions/notification';

import BasicModal from './modals/basic';
import EducationModal from './modals/educations';
import ResumeModal from './modals/resume';
import ExtraModal from './modals/extra';
import PhotoEditorModal from './modals/photo-editor';

import {
  BASIC_MODAL,
  DEGREE_MODAL,
  RESUME_MODAL,
  EXTRA_MODAL,
  PHOTO_EDITOR,
} from '../stores/mutation-types';

class PopupModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: '',
      modalToRender: '',
    };
  }

  componentWillReceiveProps(nextProps) {
    const { type } = nextProps.modal;

    switch (type) {
      case BASIC_MODAL:
        return this.setState({
          modalToRender: <BasicModal />,
          title: 'Persoonsgegevens',
        });

      case DEGREE_MODAL:
        return this.setState({
          modalToRender: <EducationModal />,
          title: 'Wat zijn je opleiding(en)',
        });

      case RESUME_MODAL:
        return this.setState({
          modalToRender: <ResumeModal />,
          title: 'MIJN CV',
        });

      case EXTRA_MODAL:
        return this.setState({
          modalToRender: <ExtraModal />,
          title: 'Extra',
        });
      case PHOTO_EDITOR:
        return this.setState({
          modalToRender: <PhotoEditorModal
            onImageCrop={ this.props.cropImage }
          />,
          title: 'Profielfoto',
        });
      default:
        return false;
    }
  }

  handleModalSubmit(values) {
    console.error('THIS METHOD IS DEPRECATED -> Move handleSubmit into the Modal itself.');
  }

  render() {
    const { toggleModal } = this.props;
    const animationTiming = 350;

    return (
      <div>
        <ReactModal
          isOpen={ this.props.modal.visible }
          onRequestClose={ () => toggleModal() }
          contentLabel="Popup Modal"
          closeTimeoutMS={animationTiming}
          shouldCloseOnOverlayClick={false}
          className={{
            base: 'popup container',
            afterOpen: 'popup--appear',
            beforeClose: 'popup--leave',
          }}
        >
          <div className="popup__body">
            <div className="popup__close">
              <button className="popup__btn-close" onClick={ () => toggleModal() }>
                <i className="nyc-icon nyc-icon-close-cross"></i>
              </button>
            </div>
            <div className="row">
              <div className="col-xs-12 col-sm-4 col-md-3">
                <h3 className="popup__title">{ this.state.title }</h3>
              </div>

              { this.state.modalToRender }
            </div>
          </div>
        </ReactModal>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    modal: state.modal,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    toggleModal: (state, payload) => dispatch(toggleModal(state, payload)),
    sendNotification: (message, level) => dispatch(addNotification(message, level)),
  };
}

PopupModal.propTypes = {
  attributes: PropTypes.object.isRequired,
  toggleModal: PropTypes.func,
  sendNotification: PropTypes.func,
  modal: PropTypes.shape({
    type: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.bool,
    ]),
    visible: PropTypes.bool.isRequired,
  }),
};

export default connect(mapStateToProps, mapDispatchToProps)(PopupModal);
