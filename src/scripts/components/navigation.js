import React from 'react';
import { Link } from 'react-router-dom';

const listStyle = {
  display: 'flex',
  width: '300px',
  justifyContent: 'space-around',
};

const Navigation = () => (
  <nav>
    <ul style={listStyle}>
      <li>
        <Link
          to={ { pathname: '/kandidaat/profiel', search: '?new_candidate_profile=2' } }>
          Dashboard
        </Link>
      </li>
      <li>
          <Link
            to={ { pathname: '/kandidaat/profiel/mijn-account', search: '?new_candidate_profile=2' } }>
          Settings
          </Link>
        </li>
      <li>
        <Link
          to={ { pathname: '/kandidaat/profiel/persoonlijke-informatie', search: '?new_candidate_profile=2' } }>
          Personal
         </Link>
      </li>
    </ul>
  </nav>
);

export default Navigation;
