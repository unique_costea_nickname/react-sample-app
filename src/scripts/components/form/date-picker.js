import React, { Component } from 'react';
import PropTypes from 'prop-types';

import DatePicker from 'react-datepicker';
import moment from 'moment';


class DatePickerWrapper extends Component {
  constructor(props) {
    super(props);

    const { input: { value, onChange } } = this.props;
    this.state = {
      date: moment(value || Date.now()),
    };

    this.handleChange = this.handleChange.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      date: moment(nextProps.input.value || Date.now()),
    });
  }

  handleChange(date) {
    this.setState({
      date,
    });

    this.props.input.onChange(moment(date).format('YYYY-MM-DD'));
  }

  render() {
    return <DatePicker
      dateFormat="DD MMMM YYYY"
      selected={this.state.date}
      className="input__text text--capitalize"
      onChange={this.handleChange}
      showMonthDropdown
      showYearDropdown
      {...this.props.custom}
    />;
  }
}

DatePicker.propTypes = {
  input: PropTypes.shape({
    value: PropTypes.string,
    onChange: PropTypes.func,
  }),
};

export default DatePickerWrapper;
