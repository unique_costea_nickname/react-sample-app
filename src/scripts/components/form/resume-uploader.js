import React from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form';
import { renderInputFile } from '../form/render-fields';

const ResumeUploadField = (props) => {
  let inputField = null;
  const { resumeAttributes } = props;
  const fileName = resumeAttributes.resume_present ? 'Click to download your resume' : 'Upload your resume';

  const openFileInput = () => {
    inputField.click();
  };

  return (
    <div className="row">
      <div className="col-xs-12">
        <div className={props.className}>
          <label htmlFor="resume">
            <div className="resume__file base__block--max-width">
              <a href={props.resumeAttributes.resume} className="base__link" download>{fileName}</a>
            </div>
            <div className="hidden">
              <Field
                name="resume"
                component={renderInputFile}
                value=''
                accept=",.txt,.pdf,.msword,.rtf,.x-rtf,.doc,.docx,.x-soffice,.octet-stream"
                inputRef={(inputFile) => { inputField = inputFile; }}
                withRef={true}
              />
            </div>
          </label>
        </div>
      </div>
      <div className="col-xs-12">
        <button type="button"
          onClick={openFileInput}
          className="btn btn--secondary btn--wide btn--modal">Vervangen CV</button>
      </div>
    </div>
  );
};

ResumeUploadField.propTypes = {
  resumeAttributes: PropTypes.object.isRequired,
  className: PropTypes.string,
};

export default ResumeUploadField;
