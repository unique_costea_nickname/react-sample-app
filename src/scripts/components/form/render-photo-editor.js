import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Cropper from 'react-cropper';

export default class PhotoEditorField extends Component {
  constructor(props) {
    super(props);

    const updatedCrop = Object.assign({}, this.props.crop);
    delete updatedCrop.imageUrl;

    this.state = {
      src: this.props.src,
      crop: updatedCrop,
    };

    this.cropUpdated = this.cropUpdated.bind(this);
    this.getData = this.getData.bind(this);
    this.initImagePreview = this.initImagePreview.bind(this);
  }

  getData() {
    if (typeof this.cropper.getCroppedCanvas() === 'undefined') {
      return this.props.crop;
    }

    const cropData = this.cropper.getCropBoxData();
    cropData.imageUrl = this.cropper.getCroppedCanvas().toDataURL();
    return cropData;
  }

  initImagePreview() {
    const { crop } = this.state;
    this.cropper.setCropBoxData(crop);
    this.cropUpdated();
  }

  cropUpdated() {
    const cropData = this.getData();

    this.props.onCropCompleted(cropData);
    this.props.input.onChange(cropData);
  }

  render() {
    return (
      <Cropper
        className={this.props.className}
        ref={(cropper) => { this.cropper = cropper; }}
        src={this.state.src}
        aspectRatio={this.props.aspectRatio}
        style={this.props.style}
        cropend={this.cropUpdated}
        ready={this.initImagePreview}
        minCropBoxWidth={250}
        minCropBoxHeight={300}
        zoomable={false}
      />
    );
  }
}

PhotoEditorField.propTypes = {
  src: PropTypes.string.isRequired,
  crop: PropTypes.object.isRequired,
  input: PropTypes.shape({
    onChange: PropTypes.func.isRequired,
  }),
  style: PropTypes.object,
  aspectRatio: PropTypes.number,
  onCropCompleted: PropTypes.func.isRequired,
};
