import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { FieldArray, Field } from 'redux-form';
import { renderField } from '../form/render-fields';

import DatePicker from '../form/date-picker';

export default class WorkExperience extends Component {
  constructor(props) {
    super(props);

    this.renderWorkExperienceFields = this.renderWorkExperienceFields.bind(this);
  }

  renderAddButton(fields) {
    return (
      <div className="row base__block--spaced">
        <div className="col-xs-12">
          <div className="btn--dash">
            <hr />
            <button type="button"
              onClick={() => fields.push({})}
              className="btn btn--tertiary btn--outline btn--white">Voeg werkervaring toe</button>
            <hr />
          </div>
        </div>
      </div>
    );
  }

  renderWorkExperienceFields({ fields }) {
    const { resumeAttributes, work_experiences } = this.props;
    return (
      <div>
        {fields.map((item, index) =>
          <div className="popup__box" key={ index }>
            <Field
              name={`${item}.company`}
              label="Bedrijf"
              component={ renderField }
              attributes={ resumeAttributes }
            />

            <Field
              name={`${item}.function_title`}
              component={ renderField }
              label="Functie"
              attributes={resumeAttributes}
            />

            <Field
              name={`${item}.city`}
              label='Vestigingsplaats'
              component={ renderField }
              attributes={resumeAttributes}
            />

            <div className="row middle-xs input form__input">
              <div className="col-xs-12 col-sm-4">
                <div className="form__label">Periode van</div>
              </div>
              <div className="col-xs-12 col-sm-8">
                <div className="input__datepicker">
                  <Field name={`${item}.date_start`} component={DatePicker} />
                </div>
              </div>
            </div>

            <div className="row middle-xs input form__input">
              <div className="col-xs-12 col-sm-4">
                <div className="form__label">tot</div>
              </div>
              <div className="col-xs-12 col-sm-8">
                <div className="input__datepicker">
                  <Field name={`${item}.date_end`}
                    component={DatePicker}
                    custom={{ disabled: work_experiences[index] && work_experiences[index].is_current }}
                  />
                </div>
              </div>
            </div>

            <div className="row middle-xs input form__input">
              <div className="col-xs-12 col-sm-offset-4 col-sm-8">
                <div className="row">
                  <div className="col-xs input--buttons input--square">
                    <Field name={`${item}.is_current`}
                      id={`${item}_checkbox`}
                      component='input'
                      className="input__button"
                      type="checkbox"
                      value={ resumeAttributes.is_current }
                    />
                    <label className="input__label"
                      htmlFor={`${item}_checkbox`}
                    >tot heden</label>
                  </div>
                  <div className="col-xs input--buttons input--square">
                    <input className="input__button"
                      type="checkbox" id="checkbox_id_1_2" defaultValue="value" />
                    <label className="input__label" htmlFor="checkbox_id_1_2">via uitzendbureau</label>
                  </div>
                </div>
              </div>
            </div>
          </div>)}
          { this.renderAddButton(fields) }

      </div>
    );
  }

  render() {
    return (
        <FieldArray name="work_experiences"
          rerenderOnEveryChange={true}
          component={this.renderWorkExperienceFields}
          attributes={this.props.resumeAttributes}
          work_experiences={this.props.workExperiences}
        />
    );
  }
}

WorkExperience.propTypes = {
  resumeAttributes: PropTypes.object.isRequired,
  work_experiences: PropTypes.array.isRequired,
  initialize: PropTypes.func,
};
