import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Select from 'react-select';
import { searchEducationsByNameAndEduType } from '../../actions/education';

import { searchObjectValue, getArrayIndexByObjectValue } from '../../helpers';

/* eslint-disable */

export const renderField = ({ input, label, type, meta: { touched, error, warning } }) => (
  <div className="row middle-xs input form__input">
    <div className="col-xs-12 col-sm-4">
      <label className="form__label" htmlFor={ input.name }>{ label }</label>
    </div>
    <div className="col-xs-12 col-sm-8">
      <input className="input__text input__input--modifier" {...input} placeholder={label} type={type} />
    </div>

    {touched && ((error && <span>{error}</span>) || (warning && <span>{ warning }</span>))}
  </div>
);

renderField.propTypes = {
  input: PropTypes.object,
  label: PropTypes.string,
  type: PropTypes.object,
};
/* eslint-enable */

export class renderSelect extends Component {
  constructor(props) {
    super(props);

    this.changeHandler = this.changeHandler.bind(this);
  }

  changeHandler(option) {
    this.props.input.onChange(option.value);
  }

  render() {
    const { input, custom, label } = this.props;
    const clearable = custom.clearable || false;
    return (
      <Select {...custom} onChange={this.changeHandler} value={input.value} clearable={clearable} />
    );
  }
}

renderSelect.propTypes = {
  input: PropTypes.object,
  custom: PropTypes.object,
};

export class AsyncSelect extends Component {
  constructor(props) {
    super(props);

    this.changeHandler = this.changeHandler.bind(this);
    this.loadOptions = this.loadOptions.bind(this);
  }

  changeHandler(option) {
    this.props.input.onChange(option.label);
  }

  loadOptions(value, callback) {
    if (value.length < 1) { return callback(null, { options: [] }); }
    const { eduTypeId } = this.props.custom.options;
    const lvalue = value.toLowerCase();

    return searchEducationsByNameAndEduType(lvalue, eduTypeId)
      .then(response => response.data.data.map(HENK => ({ label: HENK.attributes.name, value: HENK.id })))
      .then(json => ({ options: json }));
  }

  render() {
    const { input, custom } = this.props;
    const clearable = custom.clearable || false;
    return <Select.Async
      clearable={clearable}
      onChange={this.changeHandler}
      value={{ label: input.value }}
      loadOptions={this.loadOptions}
    />;
  }
}

AsyncSelect.PropTypes = {
  input: PropTypes.object,
  custom: PropTypes.object,
};

export const renderInputFile = props => (
  <input
    type="file"
    ref={ props.inputRef }
    accept={ props.accept }
    onChange={(e) => { props.input.onChange(e.target.files); }}
  />
);

renderInputFile.propTypes = {
  input: PropTypes.object,
  inputRef: PropTypes.func,
  accept: PropTypes.string,
};

export class RenderCheckboxList extends Component {
  constructor(props) {
    super(props);

    const selected = props.selectedLanguages;
    const limit = selected.length > props.limit ? selected.length : props.limit;

    this.state = {
      selected,
      limit,
    };

    this.onChangeHandler = this.onChangeHandler.bind(this);
  }

  componentWillMount() {
    const selectedCountries = this.state.selected;
    const countries = this.props.options.slice();

    const newList = countries.filter(
      country => selectedCountries.every(
        selectedCountry => selectedCountry.id !== country.id),
      );

    const updatedCountryList = [...selectedCountries, ...newList];
    this.setState({
      countryList: updatedCountryList,
    });
  }

  onChangeHandler(item) {
    const { input } = this.props;
    const selected = this.state.selected.slice();

    const index = getArrayIndexByObjectValue(selected, item.id, 'id');

    if (index > -1) {
      selected.splice(index, 1);
    } else {
      selected.push(item);
    }

    this.setState({
      selected,
    });

    input.onChange(selected);
  }

  removeLimit() {
    if (this.state.limit !== null) {
      this.setState({ limit: null });
    }
  }

  showMoreButton() {
    if (this.state.limit && !(this.state.limit >= this.props.options.length)) {
      return (
        <div className="row base__block--spaced">
          <div className="col-xs">
            <div className="btn--dash">
              <hr />
                <button
                  className="btn btn--tertiary btn--outline btn--white"
                  onClick={() => this.removeLimit()}
                >{ this.props.showMoreText }</button>
              <hr />
            </div>
          </div>
        </div>
      );
    }
    return null;
  }

  renderLimitedItems() {
    const { selected, countryList } = this.state;
    const checkboxType = this.props.checkboxType === 'square' ? 'input--square' : '';

    return countryList
      .filter((item, index) => !this.state.limit || index < this.state.limit)
      .map((item, key) => {
        const isInArray = searchObjectValue(selected, item.name, 'name').length;
        const isChecked = selected && isInArray;

        return (
          <li className={`input input--buttons ${checkboxType} ${this.props.childrenClassList}`} key={key}>
            <input type="checkbox" className="input__button"
              id={`${item.name}-${item.id}`}
              defaultChecked={isChecked}
              onChange={ () => this.onChangeHandler(item) }
            />
            <label className="input__label" htmlFor={`${item.name}-${item.id}`}>{item.name}</label>
          </li>
        );
      });
  }

  render() {
    return (
      <div>
        <ul className={this.props.className}>{ this.renderLimitedItems() }</ul>
        { this.showMoreButton() }
      </div>
    );
  }
}

RenderCheckboxList.propTypes = {
  selectedLanguages: PropTypes.array.isRequired,
  options: PropTypes.array.isRequired,
  input: PropTypes.object.isRequired,
  checkboxType: PropTypes.string,
  className: PropTypes.string,
  showMoreText: PropTypes.string.isRequired,
  childrenClassList: PropTypes.string,
  limit: PropTypes.number,
};

export const renderRadioGroup = (props) => {
  const { input, attributes } = props;
  const List = props.options.map((item, key) => {
    const radioId = Date.now() + key;

    return (
      <li className={`col-sm-3 input input--buttons ${props.childrenClassList}`} key={key}>
        <input type="radio"
          name={props.input.name}
          className="input__button"
          id={`radio-${radioId}`}
          defaultChecked={attributes[input.name] === item.value}
          onClick={() => props.input.onChange(item.value)} />
        <label className="input__label"
          htmlFor={`radio-${radioId}`}>{item.label}
        </label>
      </li>
    );
  });
  return <ul className={`row ${props.className}`}>{List}</ul>;
};

renderRadioGroup.propTypes = {
  input: PropTypes.object.isRequired,
  attributes: PropTypes.object.isRequired,
  options: PropTypes.array.isRequired,
  className: PropTypes.string,
  childrenClassList: PropTypes.string,
};
