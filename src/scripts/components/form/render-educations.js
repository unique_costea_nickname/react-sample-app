import React from 'react';
import PropTypes from 'prop-types';

import { Field } from 'redux-form';
import { renderSelect, AsyncSelect } from '../form/render-fields';
import DatePickerWrapper from './date-picker';

const
  defaultLabelColClass = 'col-xs-12 col-sm-4',
  defaultContentColClass = 'col-xs-12 col-sm-8';

export const EducationName = props => (
  <div className="row middle-xs input form__input" key={ props.key }>
    <div className={defaultLabelColClass}>
      <div className="form__label">Studie</div>
    </div>
    <div className={defaultContentColClass}>
      <Field name={`${props.item}.education_name`}
        type="input"
        component={ AsyncSelect }
        searchEducationNames={ props.searchEducationNames }
        className='input__text'
        custom={{ clearable: false, searchable: true, options: props.options }}
      />
    </div>
  </div>
);

/* eslint-disable */
export const EduLevel = props => (
  <div className="row middle-xs input form__input" key={props.key}>
    <div className={defaultLabelColClass}>
      <div className="form__label">Niveau</div>
    </div>
    <div className={ defaultContentColClass }>
      <Field name={`${props.item}.edu_level_id`}
        type="input"
        component={renderSelect}
        className="input__text"
        custom={{ clearable: false, searchable: false, options: props.options }}
      />
    </div>
  </div>
);

export const StartDate = props => (
  <div className="row middle-xs input form__input" key={props.key}>
    <div className={defaultLabelColClass}>
      <div className="form__label">Begin studie</div>
    </div>
    <div className={defaultContentColClass}>
      <Field
        name={`${props.item}.start_date`}
        type="input"
        className="input__text"
        component={DatePickerWrapper}
      />
    </div>
  </div>
)

export const EndDate = props => (
  <div className="row middle-xs input form__input" key={props.key}>
    <div className={defaultLabelColClass}>
      <div className="form__label">Einde studie</div>
    </div>
    <div className={defaultContentColClass}>
      <Field
        name={`${props.item}.end_date`}
        type="input"
        className="input__text"
        component={DatePickerWrapper}
      />
    </div>
  </div>
);
