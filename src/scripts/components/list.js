import React, { Component } from 'react';
import PropTypes from 'prop-types';

class List extends Component {
  constructor(props) {
    super(props);
    this.state = { limit: props.limit };

    this.handleOpenModal = this.handleOpenModal.bind(this);
  }

  handleOpenModal(e) {
    let type = this.props.data.modalType;

    if (e && e.nativeEvent.target.attributes.href) {
      e.nativeEvent.preventDefault();
      type = e.nativeEvent.target.attributes.href.value;
    }
    this.props.openModal('show', { type });
  }

  removeLimit() {
    this.setState({ limit: null });
  }

  items() {
    return this.props.items
      .filter((item, index) => !this.state.limit || index < this.state.limit)
      .map((item, index) => React.createElement(
        this.props.component, { [this.props.propName]: item, key: `listItem_${index}` }));
  }

  title() {
    if (this.props.data.title) {
      return (
        <div className="col-xs-12 col-sm-4 col-md-3 col-lg-2">
          <h3 className="title">{ this.props.data.title }</h3>
        </div>
      );
    }

    return null;
  }

  showMoreButton() {
    if (this.state.limit && !(this.state.limit >= this.props.items.length)) {
      return (
        <div className="row base__block--spaced">
          <div className="col-xs-12 col-sm-8 col-sm-offset-4 col-md-6 col-md-offset-4 col-lg-6 col-lg-offset-3">
            <div className="btn--dash">
              <hr />
                <button
                  className="btn btn--tertiary btn--outline btn--white"
                  onClick={() => this.removeLimit()}
                >{ this.props.showAllText }</button>
              <hr />
            </div>
          </div>
        </div>
      );
    }
    return null;
  }

  render() {
    return (
      <div>
        <div className="row candidate-details__group">
          { this.title() }
          <div className="col-xs-12 col-sm-8 col-sm-offset-0 col-md-6 col-md-offset-1 col-lg-6 col-lg-offset-1">
            <div className="card form candidate-details__card">
              <div className="form__edit">
                <button onClick={this.handleOpenModal} className="form__btn-edit">
                  <i className="nyc-icon nyc-icon-edit"></i>
                </button>
                <div className="card__cols">
                  { this.items() }
                </div>
              </div>
            </div>
          </div>
        </div>
        { this.showMoreButton() }
      </div>
    );
  }
}

List.propTypes = {
  items: PropTypes.array.isRequired,
  limit: PropTypes.number,
  propName: PropTypes.string,
  component: PropTypes.oneOfType([
    PropTypes.func,
    PropTypes.instanceOf(Component),
  ]),
  data: PropTypes.shape({
    title: PropTypes.string,
    keyList: PropTypes.array,
    modalType: PropTypes.string,
  }),
  openModal: PropTypes.func,
  showAllText: PropTypes.string,
};

export default List;
