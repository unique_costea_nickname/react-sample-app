import {
  REQUEST_CANDIDATE_INFO,
  RECEIVE_CANDIDATE_INFO,
  UPDATE_CANDIDATE_INFO,
  FETCH_ADDRESS_INFO,
  RECEIVED_ADDRESS,
  ADDRESS_NOT_FOUND,
  ADD_DEGREE_ITEM,
  MODIFY_DEGREE_ITEM,
  REMOVE_DEGREE_ITEM,
  RECV_ERROR,
} from '../stores/mutation-types';

function getCandidateUuid() {
  if (document.querySelector('meta[name="candidate_uuid"]')) {
    return document.querySelector('meta[name="candidate_uuid"]').content;
  }

  // eslint-disable-next-line
  return (process.env.config.development.candidate_uuid || console.error('please setup your CANDIDATE_UUID in config/development.js'));
}

function getCandidateAuthToken() {
  if (document.querySelector('meta[name="authentication_token"]')) {
    return document.querySelector('meta[name="authentication_token"]').content;
  }

  return process.env.config.development.authToken || 'dev_token';
}

const initialState = {
  isFetching: false,
  isLoaded: false,
  uuid: getCandidateUuid(),
  auth_token: getCandidateAuthToken(),
  attributes: {
    firstname: '',
    lastname: '',
    email: '',
    is_male: null,
    date_of_birth: '',
    telephone: '',
    home_address: {
      addressPresent: false,
    },
    degrees: [{}],
    picture: {
      crop: {
        height: 300,
        left: 0,
        top: 0,
        width: 250,
      },
      imageUrl: '',
      picture_present: false,
    },
    resume: {
      linkedin_link: '',
      resume: '',
      resume_present: false,
      work_experiences: [{}],
    },
    candidate_languages: [],
    has_license: false,
    has_car: false,
  },
};

export default function candidate(state = initialState, action) {
  let updatedAttrs;

  switch (action.type) {
    case REQUEST_CANDIDATE_INFO:
      return Object.assign({}, state, {
        isFetching: true,
      });
    case RECEIVE_CANDIDATE_INFO:
      updatedAttrs = Object.assign({}, state.attributes, action.payload.attributes);
      updatedAttrs.picture.imageUrl = action.payload.links.photo_url;
      updatedAttrs.resume.work_experiences = updatedAttrs.work_experiences;

      if (updatedAttrs.home_address.street) {
        updatedAttrs.home_address.addressPresent = true;
      }

      return Object.assign({}, state, {
        isFetching: false,
        isLoaded: true,
        attributes: updatedAttrs,
        uuid: action.payload.uuid,
        lastUpdated: action.payload.receivedAt,
      });
    case UPDATE_CANDIDATE_INFO:
      return Object.assign({}, state, {
        isFetching: false,
        isLoaded: true,
        attributes: action.payload.attributes,
        uuid: action.payload.uuid,
        lastUpdated: action.payload.receivedAt,
      });
    case ADDRESS_NOT_FOUND:
    case FETCH_ADDRESS_INFO:
      return Object.assign({}, state, {
        attributes: {
          ...state.attributes,
          home_address: {
            ...state.attributes.home_address,
            addressPresent: action.payload.addressPresent,
          },
        },
      });
    case RECEIVED_ADDRESS:
      return Object.assign({}, state, {
        attributes: {
          ...state.attributes,
          home_address: {
            ...action.payload.address,
            addressPresent: action.payload.addressPresent,
          },
        },
      });
    case ADD_DEGREE_ITEM:
      return Object.assign({}, state, {
        isFetching: false,
        isLoaded: true,
        attributes: action.payload.attributes,
        uuid: action.payload.uuid,
        lastUpdated: action.payload.receivedAt,
      });
    case MODIFY_DEGREE_ITEM:
      return Object.assign({}, state, {
        isFetching: false,
        isLoaded: true,
        attributes: action.payload.attributes,
        uuid: action.payload.uuid,
        lastUpdated: action.payload.receivedAt,
      });
    case REMOVE_DEGREE_ITEM:
      return Object.assign({}, state, {
        isFetching: false,
        isLoaded: true,
        attributes: action.payload.attributes,
        uuid: action.payload.uuid,
        lastUpdated: action.payload.receivedAt,
      });
    case 'IMAGE_CROPPED':
      return Object.assign({}, state, {
        attributes: {
          ...state.attributes,
          picture_attr: action.payload,
        },
      });
    default:
      return state;
  }
}
