export default (state = { visible: false }, action) => {
  let modalState = Object.assign({}, state);
  switch (action.type) {
    case 'MODAL_SHOW':
      modalState = {
        ...state.modal,
        type: action.payload.type,
        visible: true,
      };

      return modalState;

    case 'MODAL_HIDE':
      modalState = {
        ...state.modal,
        type: false,
        visible: false,
      };

      return modalState;

    default:
      return modalState;
  }
};
