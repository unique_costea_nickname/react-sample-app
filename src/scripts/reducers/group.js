import {
  BASIC_MODAL,
  DEGREE_MODAL,
  RESUME_MODAL,
  EXTRA_MODAL,
  PHOTO_EDITOR,
} from '../stores/mutation-types';

export default () => ({
  photo: {
    title: 'Profielfoto',
    modalType: PHOTO_EDITOR,
    keyList: [
      { key: 'profile_picture', label: 'Photo' },
    ],
  },
  basic: {
    title: 'Persoonsgegevens',
    modalType: BASIC_MODAL,
    keyList: [
      { key: 'firstname', label: 'Voornaam' },
      { key: 'lastname', label: 'Achternaam' },
      { key: 'is_male', label: 'Geslacht' },
      { key: 'date_of_birth', label: 'Geboortedatum' },
      { key: 'email', label: 'Email' },
      { key: 'telephone', label: 'Mobiel / Telefoon' },
      { key: 'home_address', label: 'Adres' },
    ],
  },
  education: {
    title: 'Wat zijn je opleiding(en)',
    modalType: DEGREE_MODAL,
    keyList: [
      { key: 'degrees', label: '' },
    ],
  },
  resume: {
    title: 'Mijn cv',
    modalType: RESUME_MODAL,
    keyList: [
      { key: 'resume_title', label: 'Geüploade CV' },
      { key: 'linkedin_link', label: 'LinkedIn' },
    ],
  },
  extra: {
    title: 'Extra',
    modalType: EXTRA_MODAL,
  },
});
