import {
  ADD_NOTIFICATION,
  ADD_LOADING_INDICATOR,
  UPDATE_CANDIDATE_INFO,
  RECEIVE_CANDIDATE_INFO,
} from '../stores/mutation-types';

export function notificationReducer(state = {}, action) {
  switch (action.type) {
    case ADD_NOTIFICATION:
      return Object.assign({}, state, {
        message: action.message,
        level: action.level,
      });
    default:
      return state;
  }
}

export function loadingIndicatorReducer(state = { loading: true }, action) {
  switch (action.type) {
    case ADD_LOADING_INDICATOR:
      return Object.assign({}, state, { loading: true });
    case RECEIVE_CANDIDATE_INFO:
      return Object.assign({}, state, { loading: false });
    case '@@redux-form/SET_SUBMIT_SUCCEEDED':
      return Object.assign({}, state, { loading: true });
    default:
      return state;
  }
}
