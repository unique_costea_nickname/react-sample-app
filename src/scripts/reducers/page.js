export default () => ({
  dashboard: {
    header: {
      title: 'dashboard',
      subTitle: 'subtitle',
    },
    componentName: 'Dashboard',
  },
  'persoonlijke-informatie': {
    header: {
      title: 'Profiel',
      subTitle: 'Persoonsgegevens',
    },
    componentName: 'PersonalInfo',
  },
  'mijn-account': {
    header: {
      title: 'My Settings',
      subTitle: 'blabla',
    },
    componentName: 'Settings',
  },
});
