/* eslint-disable */
export default {
  edu_types: [
    {
      'id': 1,
      'name': "Middelbare school",
      'options': {
        'require_period': false,
        'require_education': false,
        'require_school': false,
        'require_edu_level': true,
      },
    },
    {
      'id': 2,
      'value': 2,
      'name': "MBO",
      'position': 2,
      'options': {
        'require_period': true,
        'require_education': true,
        'require_school': false,
        'require_edu_level': true,
      },
      'site_group_id': 0,
      'label': 'MBO'
    },
    {
      'id': 3,
      'value': 3,
      'name': "HBO",
      'position': 3,
      'options': {
        'require_period': true,
        'require_education': true,
        'require_school': false,
        'require_edu_level': false,
      },
      'site_group_id': 0,
      'label': 'HBO'
    },
    {
      'id': 4,
      'value': 4,
      'name': 'Universiteit',
      'position': 4,
      'options':{
        'require_period': true,
        'require_education': true,
        'require_school': false,
        'require_edu_level': false,
      },
      'site_group_id': 0,
      'label': 'Universiteit',
    },
  ],

  edu_level: {
    'high_school': [
      {
        'value': 1,
        'label': 'VMBO'
      },
      {
        'value': 2,
        'label': 'HAVO'
      },
      {
        'value': 3,
        'label': 'VWO'
      },
      {
        'value': 4,
        'label': 'Anders'
      },
    ],
    'mbo': [
      {
        'value': 5,
        'label': 'MBO Niveau 1'
      },
      {
        'value': 6,
        'label': 'MBO Niveau 2'
      },
      {
        'value': 7,
        'label': 'MBO Niveau 3'
      },
      {
        'value': 8,
        'label': 'MBO Niveau 4'
      },
      {
        'value': 9,
        'label': 'MBO Niveau 5'
      },
    ]
  },
};
