export default [
  {
    id: 1,
    name: 'Engels',
  },
  {
    id: 2,
    name: 'Duits',
  },
  {
    id: 3,
    name: 'Frans',
  },
  {
    id: 5,
    name: 'Overig',
  },
  {
    id: 6,
    name: 'Arabisch',
  },
  {
    id: 7,
    name: 'Chinees',
  },
  {
    id: 8,
    name: 'Deens',
  },
  {
    id: 9,
    name: 'Fins',
  },
  {
    id: 10,
    name: 'Grieks',
  },
  {
    id: 11,
    name: 'Hebreeuws',
  },
  {
    id: 14,
    name: 'Italiaans',
  },
  {
    id: 15,
    name: 'Japans',
  },
  {
    id: 16,
    name: 'Koreaans',
  },
  {
    id: 17,
    name: 'Latijn',
  },
  {
    id: 18,
    name: 'Noors',
  },
  {
    id: 19,
    name: 'Pools',
  },
  {
    id: 20,
    name: 'Portugees',
  },
  {
    id: 21,
    name: 'Russisch',
  },
  {
    id: 4,
    name: 'Spaans',
  },
  {
    id: 22,
    name: 'Taiwanees',
  },
  {
    id: 23,
    name: 'Tsjechisch',
  },
  {
    id: 24,
    name: 'Turks',
  },
  {
    id: 25,
    name: 'Vlaams',
  },
  {
    id: 26,
    name: 'Zweeds',
  },
  {
    id: 27,
    name: 'Nederlands',
  },
  {
    id: 35,
    name: 'Fries',
  },
];
