import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import 'moment/locale/nl';

import CandidateReducer from './candidate';
import PageReducer from './page';
import GroupReducer from './group';
import ModalReducer from './modal';
import {
  notificationReducer,
  loadingIndicatorReducer,
} from './notification';

const allReducers = combineReducers({
  candidate: CandidateReducer,
  page: PageReducer,
  groups: GroupReducer,
  form: formReducer,
  modal: ModalReducer,
  notification: notificationReducer,
  loadingIndicator: loadingIndicatorReducer,
});

export default allReducers;
