import { connect } from 'react-redux';
import { fetchCandidateInfoIfNeeded } from '../actions/candidate';
import { toggleModal } from '../actions/modal';

import PersonalInfo from '../components/personal-info';

function mapStateToProps(state) {
  return {
    cards: state.groups,
    candidate: state.candidate,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    fetchCandidate: () => dispatch(fetchCandidateInfoIfNeeded()),
    toggleModal: (state, payload) => dispatch(toggleModal(state, payload)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(PersonalInfo);
