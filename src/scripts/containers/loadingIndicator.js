import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

class LoadingIndicatorContainer extends React.Component {
  componentWillReceiveProps(newProps) {
    this.setState({ loading: newProps.loading });
  }

  render() {
    if (this.props.loading) {
      return (
        <div className="spinner">
          <div className="bounce1"></div>
          <div className="bounce2"></div>
          <div className="bounce3"></div>
        </div>
      );
    }

    return (null);
  }
}

LoadingIndicatorContainer.propTypes = {
  loading: PropTypes.bool,
};

function mapStateToProps(state) {
  return {
    loading: state.loadingIndicator.loading,
  };
}

export default connect(mapStateToProps)(LoadingIndicatorContainer);
