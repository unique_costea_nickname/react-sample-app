import React from 'react';
import { connect } from 'react-redux';
import NotificationSystem from 'react-notification-system';

class NotificationContainer extends React.Component {
  componentDidMount() {
    this.notificationSystem = this.notificationSystem;
  }

  componentWillReceiveProps(newProps) {
    const { message, level } = newProps.notification;
    this.notificationSystem.addNotification({ message, level });
  }

  render() {
    return (
      <NotificationSystem style={false} ref={(c) => { this.notificationSystem = c; }} />
    );
  }
}

function mapStateToProps(state) {
  return {
    notification: state.notification,
  };
}

export default connect(mapStateToProps)(NotificationContainer);
