import { connect } from 'react-redux';

import Page from '../components/page';

function mapStateToProps(state) {
  return {
    page: state.page,
  };
}

export default connect(mapStateToProps)(Page);
