import React, { Component } from 'react';
import { Route } from 'react-router-dom';

import Page from './scripts/containers/page';

import NotificationContainer from './scripts/containers/notification';
import LoadingIndicatorContainer from './scripts/containers/loadingIndicator';

import './styles/app.scss';

class App extends Component {
  render() {
    return (
      <div>
        <div className="blur-wrapper">
          <Route
            path="/kandidaat/profiel/:pageName"
            component={
              props => <Page {...props} />
            }
          />

          <Route exact path="/kandidaat/profiel"
            component={
              props => <Page {...props} pageName="dashboard" />
            }
          />

          <Route exact path="/"
            component={
              props => <Page {...props} pageName="dashboard" />
            }
          />
        </div>

        <NotificationContainer />
        <LoadingIndicatorContainer />
      </div>
    );
  }
}

export default App;
